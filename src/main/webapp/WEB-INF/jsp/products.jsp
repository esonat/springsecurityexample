<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="pageTitle" value="All Products" scope="request"/>
<jsp:include page="includes/header.jsp"/>

<p>This shows all products</p>
<c:url var="addProductUrl" value="/add"/>
<div id="add" class="pull-right"><a href="${addProductUrl}">Add Product</a></div>
<table class="table table-bordered table-striped table-condensed">
    <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
        </tr>
    </thead>
    <tbody>
        <c:if test="${empty products}">
            <tr>
                <td colspan="2" class="msg">No products.</td>
            </tr>
        </c:if>
         <c:forEach items="${products}" var="product">
            <tr>
                <td><c:out value="${product.id}"/></td>
                <td><c:out value="${product.name}" /></td>
                <td>
                    <form action="/SpringSecurityExample/update/${product.id}" method="POST">
	              	  	<input type="submit" value="Update"/>
	                </form>
                	<form action="/SpringSecurityExample/delete/${product.id}" method="POST">
		             	<input type="submit" value="Delete"/>
	                </form>
                </td>
            </tr>
        </c:forEach>
    </tbody>
</table>
<jsp:include page="includes/footer.jsp"/>