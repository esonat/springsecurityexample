<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:set var="pageTitle" value="Add Product" scope="request"/>
<jsp:include page="includes/header.jsp"/>

<p>Add Product</p>
<c:url var="welcomeUrl" value="/add"/>
<div id="add" class="pull-right"><a href="${welcomeUrl}">Products</a></div>
<table class="table table-bordered table-striped table-condensed">
    <thead>
        <tr>
            <th>Name</th>
        </tr>
    </thead>
    <tbody>
    	<tr>
    	<td>
    		<form:form action="/SpringSecurityExample/add" method="POST" commandName="Product">
    			<form:input type="text" path="name"></form:input>
    			<input type="submit" value="Submit"/>
    		</form:form>
    	</td>
    	</tr>
    </tbody>
</table>
<jsp:include page="includes/footer.jsp"/>