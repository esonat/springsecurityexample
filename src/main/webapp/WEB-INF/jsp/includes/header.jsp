<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html lang="en">
  <head>
        <title>Products </title>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <c:url var="cssUrl" value="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"/>
        <link href="${cssUrl}" rel="stylesheet"/>
        <style>
          body {
            padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
          }
        </style>

        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    </head>
    <body>
      <div id="nav-bar" class="navbar navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <c:url var="welcomeUrl" value="/" />
                <a class="brand" href="${welcomeUrl}">Products</a>
                <div class="nav-collapse">
                    <ul class="nav">
                    	<c:url var="productsUrl" value="/" />
                        <li><a id="navEventsLink" href="${productsUrl}">All Products</a></li>
                        <c:url var="addProductUrl" value="/add" />
                        <li><a id="navMyEventsLink" href="${addProductUrl}">Add Product</a></li>
                    </ul>
                </div>
                <div id="nav-account" class="nav-collapse pull-right">
                    <ul class="nav">
                            <sec:authorize access="authenticated" var="authenticated"/>
                            <c:choose>
                                <c:when test="${authenticated}">
                                    <li id="greeting"><div>Welcome <sec:authentication property="name" /></div></li>
                                    <c:url var="logoutUrl" value="/logout"/>
                                    <li><a id="navLogoutLink" href="${logoutUrl}">Logout</a></li>
                                </c:when>
                                <c:otherwise>
                                    <c:url var="loginUrl" value="/login/form"/>
                                    <li><a id="navLoginLink" href="${loginUrl}">Login</a></li>
                                </c:otherwise>
                            </c:choose>
                    </ul>
                </div>
            </div>
        </div>

    </div>

    <div class="container">
        <c:if test="${message != null}">
            <div class="alert alert-success" id="message"><c:out value="${message}"/></div>
        </c:if>
        <h1 id="title"><c:out value="${pageTitle}"/></h1>
