package com.example.dao;

import java.util.List;
import com.example.domain.Product;

public interface ProductDao {
	List<Product> getAll();
	Product findById(int id);
	void addProduct(Product product);
	void updateProduct(int id,Product product);
	void deleteProduct(int id);	
}
