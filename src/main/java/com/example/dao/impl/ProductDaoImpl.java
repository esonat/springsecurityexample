package com.example.dao.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Repository;
import com.example.dao.ProductDao;
import com.example.domain.Product;

@Repository
public class ProductDaoImpl implements ProductDao {
	
	private List<Product> products=new ArrayList<Product>();
	private int 		  idCounter=5;
	
	public ProductDaoImpl() {
		products=new ArrayList<Product>();
		
		products.add(new Product(1,"product1"));
		products.add(new Product(2,"product2"));
		products.add(new Product(3,"product3"));
		products.add(new Product(4,"product4"));
	}
	
	@Override
	public List<Product> getAll() {
		return products;
	}

	@Override
	public Product findById(int id) {
		for(Product product:products){
			if(product.getId()==id)
				return product;
		}
		return null;
	}

	@Override
	public void addProduct(Product product) {
		product.setId(idCounter++);
		products.add(product);
	}

	@Override
	public void updateProduct(int id, Product product) {
		Product updated=findById(id);
		updated.setName(product.getName());
	}

	@Override
	public void deleteProduct(int id) {
		products.remove(findById(id));
		for(int i=0;i<products.size();i++){
			products.get(i).setId(i+1);
		}
		
		idCounter=products.size()+1;
	}	
}
