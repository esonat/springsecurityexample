package com.example.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.dao.ProductDao;
import com.example.domain.Product;
import com.sun.xml.internal.fastinfoset.algorithm.IEEE754FloatingPointEncodingAlgorithm;

@Controller
public class ProductController {
	@Autowired
	private ProductDao productDao;
	
	@RequestMapping("/")
	public String listProducts(Model model){
		model.addAttribute("products",productDao.getAll());
		return "products";
	}
	
	@RequestMapping(value="/add",method=RequestMethod.GET)
	public String getAddProduct(@ModelAttribute("Product") Product product,Model model){
		return "addProduct";
	}
	
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public String addProduct(@ModelAttribute("Product") Product product){
		productDao.addProduct(product);
		return "redirect:/";
	}
	@RequestMapping(value="/update/{id}",method=RequestMethod.GET)
	public String getUpdateProduct(@ModelAttribute("Product") Product product,
								@PathVariable int id,
								Model model){
		Product updated = productDao.findById(id);
		model.addAttribute("product",updated);
		
		return "updateProduct";
	}
	
	@RequestMapping(value="/update/{id}",method=RequestMethod.POST)
	public String updateProduct(@ModelAttribute("Product") Product product,
								@PathVariable int id){
		productDao.updateProduct(id,product);
		return "redirect:/";
	}
	
	
	@RequestMapping(value="/delete/{id}",method=RequestMethod.POST)
	public String deleteProduct(@PathVariable int id){
		productDao.deleteProduct(id);
		return "redirect:/";
	}
}
