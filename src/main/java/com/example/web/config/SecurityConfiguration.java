package com.example.web.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.access.intercept.DefaultFilterInvocationSecurityMetadataSource;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter{
	@Autowired
	private DataSource dataSource;
	
	@Override
	protected void configure(HttpSecurity http) throws Exception{
		http
		.authorizeRequests()
			.antMatchers("/").hasAnyRole("USER","ANONYMOUS")
			.antMatchers("/add").hasRole("USER")
			.antMatchers("/update/*").hasRole("ADMIN")
			.antMatchers("/delete/*").hasRole("ADMIN")
		.and()
			.formLogin()
				.loginPage("/login.jsp")
				.loginProcessingUrl("/login")
				.defaultSuccessUrl("/")
				.failureUrl("/login.jsp?error=true")
		.and()
			.logout()
				.logoutSuccessUrl("/")
		.and()
			.anonymous()
				.principal("guest")
				.authorities("ROLE_GUEST")
		.and()
			.rememberMe();
	}
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception{
			auth.inMemoryAuthentication()
			.withUser("admin").password("admin").authorities("ROLE_ADMIN","ROLE_USER")
			.and().withUser("user").password("user").authorities("ROLE_USER");
		
		  auth.jdbcAuthentication()
          .dataSource(dataSource)
          .usersByUsernameQuery(
                  "SELECT USERNAME,PASSWORD,'1' AS ENABLED FROM MEMBER WHERE USERNAME=?")
          .authoritiesByUsernameQuery(
                  "SELECT MEMBER.USERNAME,MEMBER_ROLE.ROLE AS authorities " +
                  "FROM MEMBER,MEMBER_ROLE " +
                  "WHERE MEMBER.USERNAME=? AND MEMBER.ID=MEMBER_ROLE.MEMBER_ID")
          	.passwordEncoder(new Md5PasswordEncoder());
	}
	
}
