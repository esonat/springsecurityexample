package com.example.web;

import com.example.config.BeanConfiguration;
/*import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;
*/
import com.example.web.config.SecurityConfiguration;
import com.example.web.config.WebConfiguration;

import org.springframework.core.annotation.Order;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

@Order(1)
public class ApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer{
	
/*
	@Override
	public void onStartup(ServletContext container){
		AnnotationConfigWebApplicationContext rootContext=new AnnotationConfigWebApplicationContext();
		container.addListener(new ContextLoaderListener(rootContext));
		
		AnnotationConfigWebApplicationContext dispatcherServlet=new AnnotationConfigWebApplicationContext();
		dispatcherServlet.register(WebConfiguration.class);
		
		ServletRegistration.Dynamic dispatcher=container.addServlet("dispatcher",new DispatcherServlet(dispatcherServlet));
		dispatcher.setLoadOnStartup(1);
		dispatcher.addMapping("/");
	}*/
	
	protected Class<?>[] getRootConfigClasses(){
		return new Class<?>[] {BeanConfiguration.class,SecurityConfiguration.class};		
	}
	
	protected Class<?>[] getServletConfigClasses(){
		return new Class<?>[] {WebConfiguration.class};	
	}
	
	protected String[] getServletMappings(){
		return new String[] {"/"};
		
	}
}
